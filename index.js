const express = require("express");
const router = express();
const port = 3000;
const errorHandler = require("./errorHandler");
const routes = require("./route/app/user.route");
const http = require('http')

router.use(express.urlencoded({ extended: false }));
router.use(express.json());

router.use(routes);
router.use(errorHandler);
router.listen(port, () => {
  console.log(`Example router listening on port ${port}`);
});
