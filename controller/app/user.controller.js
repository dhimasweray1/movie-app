const { User} = require('../../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);


const { sequelize } = require('../../models')

class UserController {
  static async register(req, res,next) {
    try {
            req.body.password = bcrypt.hashSync(req.body.password, 10);
        const user = await User.create({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
      })
      res.status(201).json({
        message: 'Successfully create user'
      })
    } catch (error) {
      next(error)
     }

   }

   static async login (req, res, next) {
     try {
      let token;
      const user = await User.findOne({
        where: {
          username: req.body.username
        }
      })
      if (!user) {
        throw {
          status: 401,
          message: 'Invalid username or password'
        }
      } else if (bcrypt.compareSync(req.body.password, user.password)) {
          // mengeluarkan token
          token = jwt.sign({ id: user.id, name: user.username },'sshhhh');
      }else if (req.body.google_id_token) {
        const payload = await client.verifyIdToken({
          idToken: req.body.google_id_token,
          audiance: process.env.GOOGLE_CLIENT_ID,
        });

        const user = await User.findOne({
          where: { email: payload.payload.email, auth: "google" },
        });
        if (user) {
         token = jwt.sign({ id: user.id, name: user.username },'sshhhh');
        } else {
          const createdUser = await User.create({
            email: payload.payload.email,
            auth: "google",
          });
          token = jwt.sign({ id: user.id, name: user.username },'sshhhh');
        }
      } 
      else {
        throw {
          status: 404,
          message: "Invalid email or password",
        };
      }
      res.status(200).json({
        message: "Login Succes",
        token
      });
     }catch (error) {
      next(error);
     }
   }

};

module.exports = UserController;