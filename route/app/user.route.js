const UserController = require('../../controller/app/user.controller');
const { body, validationResult } = require('express-validator');
const router = require("express").Router();
const wishlistRoutes = require("./wishlist.route");


router.use("/wishlist", wishlistRoutes);
router.post('/register',
[
  body('username')
    .notEmpty()
    .withMessage('Name should not be empty')
    ,
  body('password')
    .notEmpty()
    .withMessage('Name should not be empty'),
  body('email')
    .notEmpty()
  .withMessage('Name should not be empty')
],
(req, res, next) => {
const errors = validationResult(req);
if (!errors.isEmpty()) {
  next({
    status: 400,
    message: errors.array()
  })
} else {
  next()
}
},
UserController.register);

router.post('/login', UserController.login)

module.exports = router;