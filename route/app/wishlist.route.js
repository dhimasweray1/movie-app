const UserController = require("../../controller/app/user.controller");
const WishlistController = require("../../controller/app/wishlist.controller");
const jwt = require('jsonwebtoken')


const router = require("express").Router();
router.post("/",
    (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
            message: 'Unauthorized request'
          }
        }
    }
  } catch (err) {
    next(err)
  }
}, WishlistController.add);

router.get("/",
 (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
            message: 'Unauthorized request'
          }
        }
    }
  } catch (err) {
    next(err)
  }
},
    WishlistController.get);
router.delete("/:id",  WishlistController.delete);

module.exports = router;
