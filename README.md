# title router

title router adalah sebuah aplikasi di mana user dapat melihat daftar film yang akan ditayangkan, serta menambahkan film yang mereka sukai ke dalam daftar wishlist mereka.

## Ketentuan Aplikasi

- Terdapat fitur register
- Terdapat fitur login
  Fitur login harus menggunakan strategi JWT. Login dapat dilakukan dengan memasukkan email/username dan password serta google dan facebook OAuth
- User dapat melihat daftar film yang akan ditayangkan tanpa harus login
- User dapat menambahkan film mereka ke dalam daftar wishlist (harus login)
- User dapat menampilkan daftar wishlist mereka (harus login)
- User dapat menghapus film dari wishlist mereka (harus login)

## Lainnya

- Semua kredensial (google client ID, google client secret, JWT secret WAJIB dimasukkan ke dalam .env)
- Field semua tabel dibebaskan
- Tidak perlu membuat API CRUD title, tapi WAJIB membuat data seeder untuk title

## Ketentuan Pengerjaan

- Push kodingan ke gitlab dan kumpulkan linknya ke google form
- Tidak perlu menggunakan testing
- Deadline 19 Mei 2022, pukul 19.00